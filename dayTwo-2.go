package main

import (
	"fmt"
	// "errors"
	"math/rand"
)

type ProcesingNilai interface{
	logging()
	randomize()
	countAll() (int, int, int, int)
}

 type Hasil struct {
	 total int
	 min int
	 max int
	 average int
 }

func logging(){
	fmt.Println("Selesai memanggil function")
	message := recover()
	if message != nil {
		fmt.Println("terjadi error", message)
	}
}

func randomize(slice []int) {
	defer logging()
	for i, v := range slice {
		pembagi := rand.Intn(3)
		if pembagi == 0 {
			v = 100
		} else {
			v /= pembagi
		}
	   	fmt.Println("index ", i, " value ", v)
   }
}

func countAll(slice []int)(int, int, int, int){
	min := slice[0]
	max := slice[0]
	var total, average int
	for i, v := range slice {
	   fmt.Println("index ", i, " value ", v)
	   total += v
	   if v < min {
		   min = v
	   }
	   if v > max {
		   max = v
	   }
   }
   average = total/len(slice)
   return min, max, total, average
 }


func main(){
 	var nilai = [...]int{
		23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12,
	 }

	 fmt.Println(nilai)

	 //kumpulan ke 1
	 fmt.Println("Ini adalah kumpulan 1:")
	 var slice1 = nilai[0:8]
	 minimal1, maksimal1, total1, average1 := countAll(slice1)
	 hasil := Hasil {
		 total: total1,
		 max: maksimal1,
		 min: minimal1,
		 average: average1,
	 }
	 fmt.Println("=======================") 
	 fmt.Println("Setelah dibagi nilai random:") 
	 fmt.Println("=======================") 
	 randomize(slice1)
	 fmt.Println("nilai minimal kumpulan 1 :", hasil.min)
	 fmt.Println("nilai maksimal kumpulan 1 :",hasil.max)
	 fmt.Println("nilai total kumpulan 1 :",hasil.total)
	 fmt.Println("nilai rata-rata kumpulan 1 : ", hasil.average)
	 
	

	//  kumpulan ke 2
	fmt.Println("\nIni adalah kumpulan 2:")
	 var slice2 = nilai[8:16]
	 minimal2, maksimal2, total2, average2 := countAll(slice2) 
	 hasil1 := Hasil {
		total: total2,
		max: maksimal2,
		min: minimal2,
		average: average2,
	}
	fmt.Println("=======================") 
	fmt.Println("Setelah dibagi nilai random:") 
	fmt.Println("=======================") 
	randomize(slice2)
	 fmt.Println("nilai minimal kumpulan 2 :", hasil1.min)
	 fmt.Println("nilai maksimal kumpulan 2 :", hasil1.max)
	 fmt.Println("nilai total kumpulan 2 :", hasil1.total)
	 fmt.Println("nilai rata-rata kumpulan 2 : ", hasil1.average)

	 //  kumpulan ke 2
	fmt.Println("\nIni adalah kumpulan 3:")
	var slice3 = nilai[16:24]
	minimal3, maksimal3, total3, average3 := countAll(slice3) 
	 hasil2 := Hasil {
		total: total3,
		max: maksimal3,
		min: minimal3,
		average: average3,
	}
	fmt.Println("=======================") 
	fmt.Println("Setelah dibagi nilai random:") 
	fmt.Println("=======================") 
	randomize(slice3)
	fmt.Println("nilai minimal kumpulan 3 :", hasil2.min)
	 fmt.Println("nilai maksimal kumpulan 3 :", hasil2.max)
	 fmt.Println("nilai total kumpulan 3 :", hasil2.total)
	 fmt.Println("nilai rata-rata kumpulan 3 : ", hasil2.average)

	// // nilai tertinggi
	if hasil2.total < hasil.total && hasil.total > hasil1.total {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 1", slice1, "dengan total", total1)
	} else if hasil2.total < hasil1.total && hasil1.total > hasil1.total {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 2", slice2, "dengan total", total2)
	} else {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 3", slice3, "dengan total", total3)
	}

	// //nilai terendah
	if hasil2.total > hasil.total && hasil.total < hasil1.total {
		fmt.Println("kumpulan dengan total terendah adalah slice 1", slice1, "dengan total", total1)
	} else if hasil2.total > hasil1.total && hasil1.total < hasil2.total {
		fmt.Println("kumpulan dengan total terendah adalah slice 2", slice2, "dengan total", total2)
	} else {
		fmt.Println("kumpulan dengan total terendah adalah slice 3", slice3, "dengan total", total3)
	}
}